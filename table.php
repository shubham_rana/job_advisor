<!doctype>
<html>
<head>
<meta charset="utf-8">
<title>table</title>
<link href="css/bootstrap.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet"> 
<style>
  body
  {
  font-family: 'Lora', serif ;
  }
  </style>
</head>
<body>  
<div class="container">
<div class="row">
<div class="col-md-12 px-0 text-center">
<h2 class="mt-3">Latest Job Positions</h2>
<p class="mt-4">Browse the recent vacancies added by companies from all over the USA. Our database is updated every hour with hundreds of new job positions, which might interest you.</p>
</div>
<div class="col-md-12 px-0">
<table class="table  table-hover table-light mt-3 " >
<thead>
<tr>
<th>Date</th>
<th>Company</th>
<th>Job Vacancy</th>
<th>City</th>
<th>Salary</th>
<th>Employment</th>
</tr>
</thead>
<tbody>
<tr>
<td>Apr 27, 2017</td>
<td> <img src="1.jpg" height="50" width="65"> </td>
<td>Developer</td>
<td>New York</td>
<td>$6000</td>
<td>Full Time </td>
</tr>
<tr>
<td>Apr 27, 2017</td>
<td><img src="2.jpg" height="50" width="65"></td>
<td>Designer</td>
<td>San Francisco</td>
<td>$5500</td>
<td>Full Time </td>
</tr>
<tr>
<td>Apr 27, 2017</td>
<td><img src="3.jpg" height="50" width="65"></td>
<td>Manager</td>
<td>Chicago</td>
<td>$6000</td>
<td>Full Time </td>
</tr>
<tr>
<td>Apr 27, 2017</td>
<td><img src="4.jpg" height="50" width="65"></td>
<td>Marketer</td>
<td>New York</td>
<td>$4000</td>
<td>Full Time </td>
</tr>
<tr>
<td>Apr 27, 2017</td>
<td><img src="5.jpg" height="50" width="65"></td>
<td>Developer</td>
<td>Boston</td>
<td>$5500</td>
<td>Full Time </td>
</tr>
<tr>
<td>Apr 27, 2017</td>
<td><img src="6.jpg" height="50" width="65"></td>
<td>Art Director</td>
<td>San Diego</td>
<td>$6000</td>
<td>Full Time </td>
</tr>
</tbody>
</table>
<div class="text-center">
<input type="submit" value="VIEW ALL JOB POSITIONS" class="btn btn-outline-success mt-4"/>	 
</div>
</div>
</div>
</div>
</body>
</html>
