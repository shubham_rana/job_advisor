<!DOCTYPE html>
<html>
<head>
    <title>login</title>
    <script src="jquery.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
  <link href="css/bootstrap.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet"> 
  <style>
  body
  {
  font-family: 'Lora', serif ;
  }
  input
  {
      height: 50px;
  }
  </style>
</head>
<body class="bg-dark">
<div class="container-fluid">
 <div class="row">
  <div class="col-md-4 bg-dark mx-auto mt-2 p-5">
   <h2 class="text-white">Registration</h2>
    <form action="" method="post">
    <input type="text" name="username" value="" class="form-control mt-4" placeholder="Username"/>
    <input type="email" name="username" value="" class="form-control mt-4" placeholder="E-mail"/>
    <input type="password" name="password" value="" class="form-control mt-4" placeholder="Password"/>
    <input type="password" name="password" value="" class="form-control mt-4" placeholder="Confirm Password"/>
    <input type="submit" class="btn btn-success btn-block mt-4" value="CREATE AN ACCOUNT" />
   <div class="row mt-4">
    <div class="col-md-4">
     <hr class="border-light"/>
    </div>
    <div class="col-md-4">
     <p class="text-light">or enter with</p>
    </div>
    <div class="col-md-4">
     <hr class="border-light"/>
    </div>
   </div>
</form>
<div class="btn-group btn-group-lg ">
  <button type="button" class="btn btn-primary ml-4 px-5" style="background: blue"><i class="fab fa-facebook"></i></button>
  <button type="button" class="btn btn-primary px-5"><i class="fab fa-twitter-square"></i></button>
  <button type="button" class="btn btn-danger px-5"><i class="fab fa-google"></i></i></button>
</div> 
</div>
</div>
</div>
</body>
</html>