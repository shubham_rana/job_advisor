    <!DOCTYPE html>
<html>
<head>
    <title>login</title>
    <script src="jquery.js"></script>
  <link href="css/bootstrap.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet"> 
  <style>
  body
  {
  font-family: 'Lora', serif ;
  }
  input
  {
      height: 50px;
  }
  </style>
</head>
<body class="bg-dark">
<div class="container-fluid">
<div class="row">
<div class="col-md-4 bg-dark mx-auto mt-2 p-5">
<h2 class="text-white">Sign In</h2>
<form action="" method="post">
<input type="email" name="username" value="" class="form-control mt-4" placeholder="E-mail"/>
<input type="password" name="password" value="" class="form-control mt-4" placeholder="Password"/>
<input type="submit" class="btn btn-success mt-4" value="SIGN IN" />
</form>
</div>
</div>
</div>
</body>
</html>